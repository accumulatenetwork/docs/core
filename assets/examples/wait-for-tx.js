const api = "https://kermit.accumulatenetwork.io/v3";
const waitTime = 500;
const waitLimit = 10000 / waitTime;

async function waitForAll(txid) {
    const { produced } = await waitForSingle(txid);
    if (!produced?.records) {
        return;
    }

    for (const { value: txid } of produced.records) {
        await waitForAll(txid);
    }
}

async function waitForSingle(txid) {
    for (let i = 0; i < waitLimit; i++) {
        const resp = await fetch(api, {
            method: 'POST',
            body: JSON.stringify({
                jsonrpc: "2.0",
                id: 1,
                method: "query",
                params: {
                    scope: txid
                }
            })
        })
        const { result, error } = await resp.json()

        if (result?.statusNo == 200 || error?.code == -33404) {
            // Pending
            await new Promise(r => setTimeout(r, waitTime));
            continue;
        }

        if (error) {
            throw new Error(`query failed: ${error.message}`);
        }

        if (result.statusNo == 201) {
            // Delivered
            console.log(`${txid} was delivered`)
            return result;
        }

        if (result.statusNo >= 400) {
            // Failed
            throw new Error('transaction failed');
        }

        throw new Error('unknown status');
    }

    throw new Error(`transaction still missing or pending after ${waitTime*waitLimit/1000} seconds`)
}
