# Accounts

## Lite Accounts

### Lite identity

### Lite token account

### Lite data account

## ADI accounts

### Identity (ADI)

### Key books

### Key page

### Token account

### Data account

### Token issuer

## System accounts

### System ledger

### Block ledger

### Synthetic ledger

### Anchor ledger
