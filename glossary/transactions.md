# Transactions

## User transactions

Unless otherwise noted, the principal of a transaction must be an existing account, meaning the transaction will fail if the principal does not exist.

### Create Identity

Creates a root ADI or a sub-ADI.

* When creating a sub-ADI, the principal must be the ADI that will be its parent. For example, when creating a sub-ADI named `acc://alice/home`, the principal must be `acc://alice`. In this case CreateIdentity creates the new ADI directly without producing a synthetic transaction.
* When creating a root ADI, the principal may be the ADI being created. For example, when creating an ADI named `acc://alice`, the principal may be `acc://alice`. In this case the principal _must not_ exist and CreateIdentity creates the new ADI directly without producing a synthetic transaction.
* When creating a root ADI, the principal may be any other existing account. For example, when creating an ADI named `acc://alice`, the principal may be a lite token account. In this case CreateIdentity _does not_ create the new ADI and instead produces a SyntheticCreateIdentity.

## Synthetic transactions

### Synthetic Create Identity

### Synthetic Write Data

### Synthetic Deposit Tokens

### Synthetic Deposit Credits

### Synthetic Burn Tokens

## System transactions

### System Genesis

The transaction that activated Accumulate. Accounts that exist in block 1 were created by the genesis transaction, such as the system, synthetic, and anchor ledgers and imported Factoid accounts. An Accumulate network will always have exactly one genesis transaction, executed at block 1.

### Directory Anchor

An anchor sent from the Directory Network to the other partitions.

### Block Validator Anchor

An anchor sent from a Block Validator Network to the Directory Network.
