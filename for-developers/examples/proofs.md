# Proofs

## Prove a transaction

Prove [68f30ce91773ed3a3dbd9f52ddfe7ae22f546467249612e76663d8fe39504c47](https://explorer.accumulatenetwork.io/acc/68f30ce91773ed3a3dbd9f52ddfe7ae22f546467249612e76663d8fe39504c47@inv-icp-adi-2.acme/deal-logs).

### Get a BVN receipt for the transaction's main chain entry

```shell
TX="68f30ce91773ed3a3dbd9f52ddfe7ae22f546467249612e76663d8fe39504c47@inv-icp-adi-2.acme/deal-logs"
curl https://mainnet.accumulatenetwork.io/v3 -X POST -s --data-raw '{"jsonrpc": "2.0", "id": 1, "method": "query", "params": {"scope": "'$TX'", "query": {"queryType": "chain", "includeReceipt": true}}}' | jq .result.records[0]
```

```json
{
  "recordType": "chainEntry",
  "account": "acc://inv-icp-adi-2.acme/deal-logs",
  "name": "main",
  "type": "transaction",
  "index": 142587,
  "entry": "68f30ce91773ed3a3dbd9f52ddfe7ae22f546467249612e76663d8fe39504c47",
  "receipt": {
    "start": "68f30ce91773ed3a3dbd9f52ddfe7ae22f546467249612e76663d8fe39504c47",
    "end": "68f30ce91773ed3a3dbd9f52ddfe7ae22f546467249612e76663d8fe39504c47",
    "anchor": "83d6c876589eaa28dd9a1270ebc4063e250ceefb4f7cdf02a6f9fde13bf11e13",
    "entries": [ /* ... */ ],
    "localBlock": 19965385,
    "localBlockTime": "2024-04-10T19:53:10Z"
  },
  "state": [ /* ... */ ]
}
```

### Get a directory receipt for the BVN anchor

```shell
ANCHOR="83d6c876589eaa28dd9a1270ebc4063e250ceefb4f7cdf02a6f9fde13bf11e13"
curl https://mainnet.accumulatenetwork.io/v3 -X POST -s --data-raw '{"jsonrpc": "2.0", "id": 1, "method": "query", "params": {"scope": "dn.acme/anchors", "query": { "queryType": "anchorSearch", "anchor": "'$ANCHOR'", "includeReceipt": true }}}' | jq .result.records[0]
```

```json
{
  "recordType": "chainEntry",
  "name": "anchor(chandrayaan)-root",
  "type": "anchor",
  "index": 138397,
  "entry": "83d6c876589eaa28dd9a1270ebc4063e250ceefb4f7cdf02a6f9fde13bf11e13",
  "receipt": {
    "start": "83d6c876589eaa28dd9a1270ebc4063e250ceefb4f7cdf02a6f9fde13bf11e13",
    "end": "83d6c876589eaa28dd9a1270ebc4063e250ceefb4f7cdf02a6f9fde13bf11e13",
    "anchor": "c63394eee75724c3fd01c124576ecf6ddafae817a534b99aa2f208c7c8f3872e",
    "entries": [ /* ... */ ],
    "localBlock": 21633361,
    "localBlockTime": "2024-04-10T19:53:29Z"
  },
  "state": [ /* ... */ ]
}
```

### Get a directory receipt ending at a specific height

{% hint style="warning" %}
Not yet available on MainNet
{% endhint %}

The `includeReceipt` supports fetching an anchor that ends at a specific height of the root anchor chain. For example, replacing `"includeReceipt": true` with `"includeReceipt": { "forHeight": 123 }`  attempts to return a receipt anchored at root chain entry 123.

### Get a receipt for a specific DN anchor

{% hint style="warning" %}
Not available yet on MainNet
{% endhint %}

1. Given a DN anchor

   ```shell
   curl http://127.0.1.1:26660/v3 -X POST -s --data-raw '{"jsonrpc": "2.0", "id": 1, "method": "query", "params": {"scope": "dn.acme/anchors", "query": { "queryType": "chain", "name": "main", "index": 15 }}}' | jq .result.value.message.transaction
   ```

   ```json
   {
     "header": {
       "principal": "acc://dn.acme/anchors"
     },
     "body": {
       "type": "directoryAnchor",
       "source": "acc://dn.acme",
       "minorBlockIndex": 44,
       "rootChainIndex": 72,
       "rootChainAnchor": "21b157d9d0fe394c3cedcfdd3dc77a32b88c74185440d9c5c3e98e0f1d33becd",
       "stateTreeAnchor": "a74671f84126e3ed604bbd6f17523646bf88e24539db5fc81052278c591e52a8",
       "makeMajorBlockTime": "0001-01-01T00:00:00Z"
     }
   }
   ```

2. And a transaction

   ```shell
   curl http://127.0.1.1:26660/v3 -X POST -s --data-raw '{"jsonrpc": "2.0", "id": 1, "method": "query", "params": {"scope": "5eee79bf10e13d26532067ff93473aced45e68942f4cc58e/ACME", "query": { "queryType": "chain", "name": "main", "index": 0, "includeReceipt": true }}}' | jq .result.recei
   ```

   ```json
   {
     "start": "142893cf5282cfdacc6860226049c39f892e87e74153dc1a633cd41f847eef5b",
     "end": "142893cf5282cfdacc6860226049c39f892e87e74153dc1a633cd41f847eef5b",
     "anchor": "63891df81e9cd2b94a1027eb5d3d05554aff9824a4bcb24da44c2448e7f2a8a9",
     "entries": [ /* ... */ ],
     "localBlock": 18,
     "localBlockTime": "2024-04-18T17:51:48Z"
   }
   ```

3. Fetch a receipt starting with the previous receipt's anchor and ending at the target anchor's root chain index

   ```shell
   curl http://127.0.1.1:26660/v3 -X POST -s --data-raw '{"jsonrpc": "2.0", "id": 1, "method": "query", "params": {"scope": "dn.acme/anchors", "query": { "queryType": "anchorSearch", "anchor": "63891df81e9cd2b94a1027eb5d3d05554aff9824a4bcb24da44c2448e7f2a8a9", "includeReceipt": { "forHeight": 72 } }}}' | jq .result.records[0].receipt
   ```
   
   ```json
   {
     "start": "63891df81e9cd2b94a1027eb5d3d05554aff9824a4bcb24da44c2448e7f2a8a9",
     "end": "63891df81e9cd2b94a1027eb5d3d05554aff9824a4bcb24da44c2448e7f2a8a9",
     "anchor": "21b157d9d0fe394c3cedcfdd3dc77a32b88c74185440d9c5c3e98e0f1d33becd",
     "entries": [ /* ... */ ],
     "localBlock": 44,
     "localBlockTime": "2024-04-18T17:52:15Z"
   }
   ```