# Examples

## Wait for a transaction to complete

```runkit nodeVersion="18.x.x"
const fetch = require("node-fetch@2.7.0");

const api = "https://kermit.accumulatenetwork.io/v3";
const waitTime = 500;
const waitLimit = 10000 / waitTime;

async function waitForAll(txid) {
    const result = await waitForSingle(txid);
    if (!result.produced || !result.produced.records) {
        return;
    }

    for (const { value: txid } of result.produced.records) {
        await waitForAll(txid);
    }
    
    return result;
}

async function waitForSingle(txid) {
    for (let i = 0; i < waitLimit; i++) {
        const resp = await fetch(api, {
            method: 'POST',
            body: JSON.stringify({
                jsonrpc: "2.0",
                id: 1,
                method: "query",
                params: {
                    scope: txid
                }
            })
        })
        const { result, error } = await resp.json()

        if (result && result.statusNo == 200 || error && error.code == -33404) {
            // Pending
            await new Promise(r => setTimeout(r, waitTime));
            continue;
        }

        if (error) {
            throw new Error(`query failed: ${error.message}`);
        }

        if (result.statusNo == 201) {
            // Delivered
            console.log(`${txid} was delivered`)
            return result;
        }

        if (result.statusNo >= 400) {
            // Failed
            throw new Error('transaction failed');
        }

        throw new Error('unknown status');
    }

    throw new Error(`transaction still missing or pending after ${waitTime*waitLimit/1000} seconds`)
}
waitForAll('acc://c5be211b2929c0afe3c15603e84795b20966d9cb1c252ecf8c010e663d616fa9@7a6f9db5789710a6b27e0c5965e337d8fc7431075290434d/ACME')
```
