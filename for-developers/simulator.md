# Simulator

The [accumulate](https://gitlab.com/accumulatenetwork/accumulate) repository includes a simulator. The simulator runs a fully functional Accumulate engine and API, though it does not simulate Tendermint. It does simulate multiple nodes with a basic consensus mechanism, so it will catch most potential consensus failures.

## Run the simulator

The easiest way to run the simulator is via Docker:

```shell
docker run -p 26660:26660 -v /path/to/data:/data registry.gitlab.com/accumulatenetwork/accumulate/simulator
```

Alternatively, `--net=host` can be used instead of `-p 26660:26660`. **Without `-v`, all history will be lost if the simulator is stopped or restarted.** The data directory (`/data`) can be bound to an arbitrary host directory, or to a docker volume, but it must be bound to something outside of the container if the simulator's state is to be persisted.

## Using the simulator in Go programs

`simulator.New` requires four parameters: a logger, database provider, network definition, and snapshot provider. These parameters are structs, functions, or simple interfaces, so the caller may provide custom inputs if they wish, though the simulator package includes implementations.

For the database provider, the simulator package includes `MemoryDatabase`, which opens transient in-memory databases, and `BadgerDatabaseFromDirectory`, which opens persistent Badger databases in the given directory.

For the network definition, the simulator package includes `SimpleNetwork`, which defines an N x M network with N BVNs, each with M nodes; and `LocalNetwork`, which defines an N x M network with IP addresses, which allows the simulator to be made network accessible.

For the snapshot provider, the simulator package includes `Genesis` and friends, which construct genesis snapshots; `SnapshotMap`, which returns snapshots from a map provided by the caller; and `SnapshotFromDirectory`, which returns snapshots read from a directory.

`simulator.Simulator` has a number of methods to submit messages, and hook into and modify the inner workings of the systems (for testing purposes), as well as a full implementation of API v3.

**The simulator must be manually stepped.** `simulator.Simulator` has a `Step` method that must be called to execute blocks.

## Using the simulator in Go tests

The `test/harness` package defines a test harness that simplifies writing tests using the simulator. See [simulator\_test.go](https://gitlab.com/accumulatenetwork/accumulate/-/blob/main/test/simulator/simulator\_test.go) for an example of how this can be used.

## Using the simulator via the API

`simulator.Simulator` has a `ListenAndServe` method that serves API v2 and/or v3 on its various interfaces.

Alternatively, the `tools/cmd/simulator` command can be used to run the simulator and serve API v2 and v3.
