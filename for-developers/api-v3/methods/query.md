# Query

Use the `query` method to read the state of the network. `query` has the following parameters:

* `scope` is the scope of the query. This must be an account URL or a message ID. The message ID may be suffixed with an account or with `unknown`. For example:
  * `acc://accumulate.acme`
  * `acc://bbcea39f9c5cc8175fd5b12ab4b1173adf7c00755386f8a9db0801682034c67f@unknown`
  * `acc://bbcea39f9c5cc8175fd5b12ab4b1173adf7c00755386f8a9db0801682034c67f@staking.acme/governance/1`
* `query` may be used to query specific parts of an account or message's state.

{% hint style="info" %}
REST examples are relative to an endpoint such as `https://mainnet.accumulatenetwork.io`.
{% endhint %}

{% hint style="info" %}
JSON-RPC must be submitted with `POST /v3` to an endpoint, e.g. ` curl -X POST https://mainnet.accumulatenetwork.io/v3 --data-raw '<json>'`.
{% endhint %}

## Query an account

#### Parameters

Parameter         | Optional | Description
------------------|----------|------------
`account`         |          | URL of the account
`include receipt` | yes      | Include a receipt for the account state

#### REST

```http
GET /query/{account}?include_receipt={include receipt}
```

#### JSON-RPC

```json
{
    "jsonrpc": "2.0",
    "id": 1,
    "method": "query",
    "params": {
        "scope": "<account>",
        "query": {
            "queryType": "default",
            "includeReceipt": <include receipt>
        }
    }
}
```

#### Examples

<details><summary>acc://ACME</summary>

```json
{
  "recordType": "account",
  "account": {
    "type": "tokenIssuer",
    "url": "acc://ACME",
    "authorities": [
      {
        "url": "acc://dn.acme/operators",
        "disabled": true
      },
      {
        "url": "acc://staking.acme/book"
      }
    ],
    "symbol": "ACME",
    "precision": 8,
    "issued": "24452643448428198",
    "supplyLimit": "50000000000000000"
  },
  "pending": {
    "recordType": "range",
    "records": [
      {
        "recordType": "txID",
        "value": "acc://d0049b680a8acf4636e62eb46a973fc7efb0d913af0e34b06fce614d5bda495f@acme"
      }
    ],
    "start": 0,
    "total": 1
  }
}
```

</details>