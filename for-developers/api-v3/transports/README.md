# Transports

API v3 supports multiple transports:

* JSON-RPC (over HTTP)
* Custom JSON over WebSocket
* Custom binary over libp2p
