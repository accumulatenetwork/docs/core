# JSON-RPC

Accumulate supports a standards-compliant JSON-RPC implementation. For example:

```bash
curl https://mainnet.accumulatenetwork.io/v3 -X POST --data-raw '
{
  "jsonrpc": "2.0",
  "method": "query",
  "params": {
    "scope": "ACME"
  },
  "id":1
}'

# Result:
# {
#   "jsonrpc": "2.0",
#   "result": {
#     "recordType": "account",
#     "account": {
#       "type": "tokenIssuer",
#       "url": "acc://ACME",
#       "authorities": [
#         {
#           "url": "acc://dn.acme/operators",
#           "disabled": true
#         },
#         {
#           "url": "acc://staking.acme/book"
#         }
#       ],
#       "symbol": "ACME",
#       "precision": 8,
#       "issued": "24452643448428198",
#       "supplyLimit": "50000000000000000"
#     },
#     "pending": {
#       "recordType": "range",
#       "records": [
#         {
#           "recordType": "txID",
#           "value": "acc://d0049b680a8acf4636e62eb46a973fc7efb0d913af0e34b06fce614d5bda495f@acme"
#         }
#       ],
#       "start": 0,
#       "total": 1
#     }
#   },
#   "id": 1
# }
```
