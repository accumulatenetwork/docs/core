# Table of contents

* [Accumulate Core](README.md)

## Design

* [Model](design/model/README.md)
  * [Overview](design/model/overview.md)
  * [Domains](design/model/domains.md)
  * [Accounts](design/model/accounts.md)
  * [Messages](design/model/messages.md)
  * [Transactions](design/model/transactions.md)
  * [Authority and Signing](design/model/authority-and-signing.md)
  * [Chains](design/model/chains.md)
* [Architecture](design/architecture/README.md)
  * [Partitions](design/architecture/partitions.md)
  * [Intermediate anchoring](design/architecture/intermediate-anchoring.md)
  * [Directory network](design/architecture/directory-network.md)

## Implementation

* [Implementation](implementation/implementation.md)

## For Developers

* [Encoding](for-developers/encoding.md)
* [Simulator](for-developers/simulator.md)
* [API v3](for-developers/api-v3/README.md)
  * [Methods](for-developers/api-v3/methods/README.md)
    * [Query](for-developers/api-v3/methods/query.md)
  * [Transports](for-developers/api-v3/transports/README.md)
    * [JSON-RPC](for-developers/api-v3/transports/json-rpc.md)
* [Examples](for-developers/examples/README.md)
  * [Status](for-developers/examples/status.md)
  * [Proofs](for-developers/examples/proofs.md)
  * [EIP-712](for-developers/examples/eip-712.md)

## Glossary

* [Accounts](glossary/accounts.md)
* [Transactions](glossary/transactions.md)
