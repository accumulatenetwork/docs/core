# Accounts

An account in Accumulate is an entity that records transactions and has state, both mutable and immutable. See [accounts.md](../../glossary/accounts.md "mention") for a complete list.

## Identities

An identity in Accumulate is a container, the primary purpose of which is to contain other accounts. Accumulate defines two types of identities: Accumulate digital identifiers (ADIs) and Lite identities.

Accounts are split into two broad categories: ADI accounts and lite accounts. ADIs may contain ADI accounts and sub-ADIs; lite identities may contain lite token accounts. Lite identites cannot be manipulated directly as their purpose is to hold lite token accounts. Lite identities cannot contain sub-identities.

## Tokens

Accumulate natively supports custom tokens. Each type of token must be defined by a token issuer, an account that can issue that type of token. While token issuers define a ticker symbol to be used with the tokens it issues, there is no guarantee that the ticker symbol is unique, so Accumulate uses the address of the token issuer to uniquely identify the token.

Accumulate token accounts hold a single type of token, identified by the address of the token issuer and set when the token account is created.

## Data accounts

Accumulate data accounts allow recording arbitrary data. Data is written as multipart entries, either Accumulate data entries or Factom data entries. Accumulate data entries are simply an array of byte arrays. Factom data entries exist for backwards-compatability with Factom and follow its conventions for defining and hashing data entries.

## Key books and pages

Key books and pages define the key set and authorization rules for ADI accounts. A book contains one or more pages in order of priority, and a page contains one or more entries. [authority-and-signing.md](authority-and-signing.md "mention") describes books and pages in detail.
