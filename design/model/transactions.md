# Transactions

Transactions are operations that modify the state of accounts. See [transactions.md](../../glossary/transactions.md "mention") for a complete list.

## Principals and transaction flows

A transaction has a principal account that is the context within which the transaction is executed. In most cases, the principal can be considered to be the account that is executing the action. For example, when sending tokens the principal is the account that the tokens are withdrawn from; in some sense the principal is the entity that is sending the tokens. In a few cases the relationship between the transaction and its principal is less clear, but the principal still has a key role in determining how signatures are validated and how the transaction is executed.

As stated in [domains.md](domains.md "mention"), a transaction is expressly forbidden from modifying any accounts that are not local to the transaction's principal. Therefore operations that involve multiple accounts must involve multiple transactions. The protocol handles this automatically by producing 'synthetic transactions'. These transactions are called 'synthetic' because they are produced by the protocol instead of being submitted by a user or application. For example, when Alice wants to send tokens to Bob, Alice submits a SendTokens transaction with her account as the principal, and in response the protocol produces a SyntheticDepositTokens transaction with Bob's account as the principal.

A successful user transaction does not guarantee that the synthetic transactions it produces (if any) will succeed. There are situations where the user transaction succeeds but the synthetic transaction fails. In these cases, the failed synthetic transaction may issue a refund by producing another synthetic transaction. For example, if Alice submits a SendTokens transaction with an invalid recipient, the SyntheticDepositTokens transaction will fail, refunding Alice's tokens by producing a SyntheticDepositTokens with Alice's account as the principal. This specific scenario would also produce a credit refund, but understanding that requires understanding [authority-and-signing.md](authority-and-signing.md "mention").

Collectively, the user transaction, any synthetic transactions it produces, and any synthetic transactions those produce are called the **transaction flow**, as in "the transaction flow initiated by Alice's SendTokens transaction". More generally, the flow of a transaction includes every transaction that can be reached by following "A produced B" or "B was produced by A" relations from some initial transaction. Currently the protocol only produces transactions in the scenarios listed above, but future releases of the protocol will likely introduce additional ways a transaction could be produced.

## User transactions

User transactions are transactions submitted by a wallet or application on behalf of a user. Some user transactions produce one or more synthetic transactions, others do not, depending on whether the transaction flow may interact with multiple domains.

In most cases a user transaction that _may_ interact with multiple domains assumes it _will_. That is, for transactions that interact with an account other than the principal, in most cases the transaction acts as though the other account is not local to the principal regardless of whether it is. For example when a user sends tokens from a ADI token account to another account, SendTokens behaves the same, producing a SyntheticDepositTokens regardless of whether the other account is local to the account the tokens are sent from.

## Synthetic transactions

The term 'synthetic' refers to the fact that the transaction is produced by the protocol, as opposed to 'normal' (user) transactions that are submitted by a user or application. Synthetic transactions are produced by the protocol in response to the execution of some other transaction. A synthetic transaction is validated by a proof showing that it was produced by the protocol as part of a block. Thus a synthetic transaction cannot be valid if it was not produced as part of a block.

## System transactions

System transactions are somewhat similar to synthetic transactions in that they are produced by the protocol during a block (and require a proof), however system transactions are not triggered by another transaction. System transactions are primarily used for anchoring between partitions of the network.
