# Authority and Signing

Accumulate's authentication and authorization model has three major components: accounts, authorities, and signers.

{% hint style="info" %}
Rejection and abstention will be enabled in a future release of Accumulate
{% endhint %}

## Accounts

An account is governed by a set of authorities. An authority of an account may be enabled for all transactions executed against that account, or for specific types of transactions. The active authority set for a transaction is the set of the principal's authorities that are enabled for that type of transaction. A transaction is executed if and only if every authority of its active authority set accepts the transaction. Thus a transaction is rejected if any active authority rejects the transaction or explicitly abstains from responding, since that precludes unanimous acceptance.

The authority set of an ADI or ADI account is defined explicitly and is mutable. The authority set of a lite token account is defined structurally - it is governed by the lite identity it belongs to. A lite identity is governed by itself. Lite data accounts are not governed by any authority and can be written to by any account (except for another lite data account). Thus the authority set of a lite account is fixed and immutable.

## Authorities

An authority is an entity that can act as an authority of an account and defines signers that may sign on its behalf. An authority accepts or rejects a transaction when any of its signers accept or reject the transaction. An authority that defines multiple signers must also define the precedence of those signers in the case that one signer accepts and another signer rejects a transaction. If a signer explicitly abstains from responding to a transaction, it is ignored unless every signer abstains.

Accumulate defines two types of authorities: lite identities and key books. A lite identity is also a signer. A key book contains one or more key pages, numbered in order of precedence.

## Signers

A signer is an entity that can sign a transaction, accepting or rejecting it or explicitly abstaining from responding. The signing mechanisms differ depending on the nature of the signer, but in general a signer can sign a transaction directly with a key, or it can delegate to another authority.

Accumulate defines two types of signers: lite identities and key pages. A lite identity signs transactions with a specific key, determined by the URL of the lite identity.

## Key pages

Key pages are highly configurable: at its simplest a key page is a one-of-one signer that signs with a key, at its most complex a key page can model complex authorization structures. A key page defines keys and other authorities (delegates) that may sign on its behalf.

A key page must define an acceptance threshold and may define rejection, response, and block thresholds. If a key page's response threshold is set, it will not accept, reject, or abstain from (collectively, respond to) a transaction until that number of its keys and/or delegates have responded to the transaction. If a key page's block threshold is set, it will not respond to a transaction until that number of blocks after the first response by a key or delegate. If those thresholds are met and/or unset, the key book will accept the transaction once the number of keys and/or delegates that accepted it meets the acceptance threshold, or it will reject the transaction once the number of keys and/or delegates that rejected it exceeds the rejection threshold (if the rejection threshold is unset the acceptance threshold is used instead). If the key page cannot possibly meet the acceptance or rejection threshold due to a split vote and/or abstentions, the key page abstains from the transaction.

## Caveats

The protocol also defines a special signer used to sign synthetic and system transactions. This signer is one of a kind and cannot be used directly.
