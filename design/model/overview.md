# Overview

At a high level, Accumulate consists of [domains.md](./domains.md "mention") containing [accounts.md](./accounts.md "mention") that send [messages.md](./messages.md "mention") to each other.

A domain is a boundary; that is, accounts within the same domain may interact directly with each other, but accounts in different domains cannot interact directly and instead must interact by sending messages.

Every interaction between to domains must be mediated by messages. Interactions that *may* cross domain boundaries are mediated by messages. All told, almost every possible interaction is mediated by a message.

From the user perspective, a single action may consist of many messages. A message may (often does) produce one or more other messages. The simplest user action - submitting a simple, single-sig transaction - involves submitting a signature and a transaction. The user's signature produces an authority signature, which triggers the execution of the transaction, which may produce another transaction, which would require various system messages. Collectively, this is called a **message flow**.