# Messages

With few exceptions, every interaction between two accounts is mediated by a message.

### Key signatures

A key signature is a message from the user to a signer saying that the user is initiating, accepting, rejecting, or abstaining from a transaction. A key signature consists of a key type, public key, signer address, transaction hash, signature, and metadata. A key signature produces an authority signature once the signer's conditions have been met, such as the acceptance threshold.

### Transactions

A transaction is a message from the user or produced by some other message that updates the state of an account or accounts. A transaction is executed if and only if every relevant authority has accepted the transaction. A transaction may produce one more more additional transactions if it needs to interact with additional domains.

### Authority signatures

An authority signature is a message from an authority saying that the authority accepts or rejects a transaction. It is sent as soon as the conditions of the authority and any of its signers are met.

### Delegated signatures

A delegated signature is a type of key signature used when a signer has delegated its authority to another signer. A delegated signature must contain a key signature.

### Synthetic messages

When a message is produced by another message, such as a transaction producing a transaction or a key signature producing an authority signature, the produced message is called 'synthetic' because it is produced by the protocol instead of by the user. Synthetic messages do not require normal signatures - instead the protocol must be able to prove that they were produced by another message.

### Validator signatures

A validator signature is a special type of key signature produced by core protocol validators where the signer is the network itself instead of an account. Validator signatures are used to prevent malicious parties from submitting bogus synthetic messages, and they are used to validate anchors sent between partitions.
