# Model

The three fundamental elements of the Accumulate model are accounts, identities, and transactions. Accounts have state and can be modified. Identities are containers of accounts, and are themselves a type of account. Transactions are operations that modify the state of an account.
