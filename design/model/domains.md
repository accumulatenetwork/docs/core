# Domains

A domain is an atomic collection of accounts organized in a hierarchy by URL. In terms of URLs, the domain of an account is the same as the domain of a normal URL. The domain is itself an account, in most cases a lite identity or an ADI. The domain of a lite token account is a lite identity. An ADI is a domain if it is the root ADI - that is, if it is not contained with/a child of another ADI. The domain of an ADI account is the root ADI atr the top of the container tree.

There are some special accounts that are domains but cannot contain any other accounts or are otherwise atypical. [ACME](https://explorer.accumulatenetwork.io/acc/acme) is a token issuer, not an identity; however is not within anything so it is a domain. Similarly, lite data accounts exist outside of any container. However, neither of these are identities: they cannot contain other accounts.

Two accounts are said to be **local** if they belong to the same domain and **remote** if they do not. Locality relates specifically to the _root_ of an account. Two accounts that belong to different sub-ADIs are still local to each other if they have the same _root_ identity.

**Domains are siloed from each other.** A domain cannot interact directly with any other domain. More generally, two accounts cannot interact directly if they are not local to each other; that is if they do not belong to the same domain. A transaction is executed within the context of some account; a transaction is expressly forbidden from interacting with (modifying) any account that is not local to its principal. In other words, a transaction may only interact with accounts that belong to the same domain as the transaction's principal.
